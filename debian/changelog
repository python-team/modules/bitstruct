bitstruct (8.20.0-2) unstable; urgency=medium

  * Team upload.
  * Switch to autopkgtest-pkg-pybuild.

 -- Colin Watson <cjwatson@debian.org>  Sun, 02 Mar 2025 17:58:01 +0000

bitstruct (8.20.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Colin Watson <cjwatson@debian.org>  Sun, 02 Mar 2025 17:41:47 +0000

bitstruct (8.19.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Use pybuild-plugin-pyproject.

 -- Colin Watson <cjwatson@debian.org>  Mon, 27 May 2024 18:15:19 +0100

bitstruct (8.15.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Fixes FTBFS with Python 3.11 (Closes: #1023904)
  * Add missing python3-all dependency to autopkgtest.

 -- Felix Geyer <fgeyer@debian.org>  Mon, 14 Nov 2022 19:37:15 +0100

bitstruct (8.9.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 26 May 2022 10:19:46 +0100

bitstruct (8.9.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Thu, 21 Apr 2022 20:15:48 -0400

bitstruct (8.9.0-1) unstable; urgency=medium

  * Team upload

  [ Håvard Flaget Aasen ]
  * New upstream version 8.9.0
  * d/control
    - Update Standards-Version to 4.5.0
    - Bump debhelper to 12
    - Add Rules-Requires-Root: no
    - Fix lowercase python in description
    - Change architecture from all to any
    - Change build-dependency from python3-all to python3-all-dev
      (Closes: #952011)
    - Add ${shlibs:Depends}
    - Add Testsuite: autopkgtest-pkg-python
  * d/copyright
    - Update copyright year
    - Add email for Upstream-Contact
    - Use secure URI on Source in
  * Set upstream metadata fields: Bug-Database, Repository, Repository-Browse,
    Bug-Submit
  * Remove override_dh_auto_test from d/rules
  * Add hardening in d/rules
  * Add upstream testsuite as autopkgtest

  [ IOhannes m zmölnig ]
  * Use "py3versions -s" for running autopkgtests
  * Make autopkgtest executable
  * Add salsa-ci configuration

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 26 Mar 2020 17:32:03 +0100

bitstruct (3.7.0-2) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Sat, 27 Jul 2019 02:57:19 +0200

bitstruct (3.7.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Brian May ]
  * New upstream version.

 -- Brian May <bam@debian.org>  Fri, 27 Apr 2018 08:03:22 +1000

bitstruct (3.4.0-1) unstable; urgency=medium

  * New upstream version.
  * Update standards version to 4.0.0.

 -- Brian May <bam@debian.org>  Thu, 13 Jul 2017 07:34:52 +1000

bitstruct (3.1.0-1) unstable; urgency=medium

  * New upstream version.

 -- Brian May <bam@debian.org>  Wed, 06 Apr 2016 12:04:11 +1000

bitstruct (2.1.3-1) unstable; urgency=medium

  * Initial release. (Closes: #819157)

 -- Brian May <bam@debian.org>  Sat, 26 Mar 2016 15:29:22 +1100
